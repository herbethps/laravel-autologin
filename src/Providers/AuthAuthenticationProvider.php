<?php

namespace Hpsweb\Autologin\Providers;

use Auth;
use Hpsweb\Autologin\Interfaces\AuthenticationInterface;

class AuthAuthenticationProvider implements AuthenticationInterface
{
    /**
     * Log a user in through the Laravel Auth facade
     * through their user id.
     *
     * @param  int  $userId
     * @return mixed
     */
    public function loginUsingId($request, $userId)
    {
        $userId = $userId->getUserId();
        $guard = config('autologin.guard');

        if ($user = Auth::guard($guard)->getProvider()->retrieveById($userId)) {
            Auth::guard($guard)->login($user);
            if($request->get('token'))
                $token = $request->get('token');
            else
                $token = auth('api')->login($user);
            $request->session()->put('token', $token);

            $user->token = $token;
            return $user;
        }

        return null;
    }

    public function loginUsingMorph($request, $user)
    {
        $guard = config('autologin.guard_models')[$user->getUserModel()];
        $model = $user->getUserModel();

        if ($user = $model::find($user->getUserId())) {
            Auth::guard($guard)->login($user);
            if($request->get('token'))
                $token = $request->get('token');
            else
                $token = auth(config('autologin.guard'))->login($user);
            $request->session()->put('token', $token);

            $user->token = $token;
            return $user;
        }

        return null;
    }
}
