<?php 

namespace Hpsweb\Autologin\Interfaces;

interface AuthenticationInterface
{
    /**
     * Log a user in through the Laravel Auth facade
     * through their user id.
     *
     * @param  int  $userId
     * @return mixed
     */
    public function loginUsingId($request, $id);
    public function loginUsingMorph($request, $id);
}
