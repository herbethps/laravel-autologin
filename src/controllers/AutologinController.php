<?php

namespace Hpsweb\Autologin;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Hpsweb\Autologin\Interfaces\AuthenticationInterface;
use Hpsweb\Autologin\Interfaces\AutologinInterface;

class AutologinController extends Controller
{
    /**
     * AuthenticationInterface provider instance.
     *
     * @var \Hpsweb\Autologin\Interfaces\AuthenticationInterface
     */
    protected $provider;

    /**
     * Studious Autologin instance.
     *
     * @var \Hpsweb\Autologin\Autologin
     */
    protected $autologin;

    /**
     * Instantiate the controller.
     *
     * @param  \Hpsweb\Autologin\Interfaces\AuthenticationInterface  $authProvider
     * @param  \Hpsweb\Autologin\Autologin                           $autologin
     * @return void
     */
    public function __construct(AuthenticationInterface $authProvider, Autologin $autologin)
    {
        $this->authProvider = $authProvider;
        $this->autologin = $autologin;
    }

    /**
     * Process the autologin token and perform the redirect.
     *
     * @param  string  $token
     * @return \Illuminate\Http\RedirecetResponse
     */
    public function autologin(Request $request, $token)
    {
        if ($autologin = $this->autologin->validate($token)) {
            // Active token found, login the user and redirect to the
            // intended path.
            $loginType = config('autologin.login_type');
            $path = "/";
            $user = (object) ['token' => null];

            if ($user = $this->authProvider->$loginType($request, $autologin)) {
                if ($request->get('url'))
                    $path = $request->get('url');
                if ($autologin->getPath() !== NULL)
                    $path = $autologin->getPath();
            }

            // Token was invalid, redirect back to the home page.
            return redirect($path)->withCookie(cookie('satellizer_token', $user->token, 0, null, null, null, false));
        }

        // Token was invalid, redirect back to the home page.
        return redirect('/');
    }
}
